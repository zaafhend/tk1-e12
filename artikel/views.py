from django.shortcuts import render, redirect
from .models import Artikel
from . import forms
from django.contrib.auth.decorators import login_required
from artikel.forms import TulisComment
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from artikel.models import Comment
import json

def artikel(request):
    liat_artikel = ''
    list_artikel = Artikel.objects.all()
    return render(request, 'artikel/index.html', {'list_artikel':list_artikel, 'liat_artikel':liat_artikel})

def show_artikel(request, slug):
    list_artikel = Artikel.objects.all()
    liat_artikel = Artikel.objects.get(slug=slug)
    list_comment = Comment.objects.filter(artikel=liat_artikel).order_by("-id")
    if request.method == "POST":
        form = TulisComment(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.artikel = liat_artikel
            comment.save()

            strJson = '[{"author":"%s","body":"%s","date":"%s","id":"%s"}]' %(comment.author.get_full_name(),comment.body,comment.date.strftime("%b. %d, %Y, %I:%M %p"), comment.id)
            responseData = json.loads(strJson)
            return JsonResponse(responseData, status=200, safe=False)
        else:
            return JsonResponse({"error":form.errors}, status=400)
    elif request.method == "GET":
        form = TulisComment()
        return render(request, 'artikel/index.html', {'list_artikel':list_artikel, 'liat_artikel':liat_artikel, 'form':form, 'list_comment':list_comment})
    else:
        return JsonResponse({"error":""}, status=400)


def delete_comment(request, id):
    comment = Comment.objects.get(id=id)
    comment.delete()
    return JsonResponse({"success":id}, safe=False)


@login_required(login_url='/login/')
def tulis(request):
    if request.method == 'POST':
        form = forms.TulisArtikel(request.POST)
        if form.is_valid():
            art = form.save(commit=False)
            art.author = request.user
            art.save()
            return redirect('artikel:index')
    else:
        form = forms.TulisArtikel()
    return render(request, 'artikel/tulis.html', {'form':form})