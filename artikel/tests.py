from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *
from artikel.forms import *
from artikel.apps import ArtikelConfig
from django.contrib.auth.models import User
from django.http import request
from artikel.models import *

class ArtikelTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('tester', '', 'password')

    def test_apps(self):
        self.assertEqual(ArtikelConfig.name, 'artikel')
        self.assertEqual(apps.get_app_config('artikel').name, 'artikel')

    def test_url_artikel_exists(self):
        response = Client().get('/artikel/')
        self.assertEqual(response.status_code,200)

    def test_artikel_url_using_func(self):
        found = resolve('/artikel/')
        self.assertEqual(found.func, artikel)

    def test_artikel_uses_base_template(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/artikel/')
        self.assertTemplateUsed(response, 'base.html')

    def test_view_dalam_artikel_kosong(self):
        response = Client().get('/artikel/')
        html_response = response.content.decode('utf8')

        self.assertIn("Tulis Blogmu", html_response)
        self.assertIn("Ayo tulis artikelmu!", html_response)

# test untuk tulis artikel
    def test_url_tulis_artikel_exists(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/artikel/tulis')
        self.assertEqual(response.status_code, 200)

    def test_tulis_artikel_uses_base_template(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/artikel/tulis')
        self.assertTemplateUsed(response, 'base.html')

    def test_tulis_url_using_func(self):
        found = resolve('/artikel/tulis')
        self.assertEqual(found.func, tulis)

    def test_view_dalam_tulis(self):
        self.client.login(username='tester', password='password')
        response = self.client.get('/artikel/tulis')
        html_response = response.content.decode('utf8')

        self.assertIn("Buat Blog", html_response)
        self.assertIn("Judul artikel", html_response)
        self.assertIn('<input type="text" name="judul" placeholder="Judul artikel" maxlength="150" required id="id_judul">', html_response)
        self.assertIn('<textarea name="isi" cols="40" rows="10" type="text" placeholder="Text artikel ..." required id="id_isi">', html_response)
        self.assertIn('<textarea name="isi" cols="40" rows="10" type="text" placeholder="Text artikel ..." required id="id_isi">', html_response)
        self.assertIn('<input id="submit_btn" type="submit" value="Submit">', html_response)

    def test_post_form(self):
        response = Client().post('/artikel/tulis/',{'judul':'','isi':''})
        self.assertIn('', response.content.decode())

    def test_model_tulis_artikel(self):
        newForm = Artikel.objects.create(judul='Lorem ipsum dolor', isi='JAKARTA, KOMPAS.com - Kasus Covid-19 di Indonesia kembali menjadi perhatian masyarakat setelah dalam dua hari terakhir, penambahan pasien dalam sehari blabla', author=self.user)
        countForm = Artikel.objects.all().count()
        self.assertEqual(countForm, 1)
        self.assertEqual(str(newForm), 'Lorem ipsum dolor')
        self.assertEqual(newForm.snippet(), 'JAKARTA, KOMPAS.com - Kasus Covid-19 di Indonesia kembali menjadi perhatian masyarakat setelah dalam dua hari terakhir, penambahan pasien dalam sehari...')

    def test_model_tulis_artikel_dengan_slug_yang_sama(self):
        newForm = Artikel.objects.create(judul='test', isi='blabla', author=self.user)
        newForm1 = Artikel.objects.create(judul='test', isi='brubru', author=self.user)
        countForm = Artikel.objects.all().count()
        response = Client().get('/artikel/detail/test/')
        response1 = Client().get('/artikel/detail/test-1/')
        self.assertEqual(response.status_code,200)
        self.assertEqual(response1.status_code,200)
        self.assertEqual(newForm.slug, 'test')
        self.assertEqual(newForm1.slug, 'test-1')
        
    def test_form_is_blank(self):
        form = TulisArtikel(data={'judul':'', 'isi':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['judul'],
            ["This field is required."]
            )
        self.assertEqual(
            form.errors['isi'],
            ["This field is required."]
            )

    def test_views_with_invalid_form(self):
        self.client.login(username='tester', password='password')
        response = self.client.post('/artikel/tulis', data={'judul':'', 'isi':''})
        self.assertIn('', response.content.decode())

    def test_views_with_valid_form(self):
        self.client.login(username='tester', password='password')
        response = self.client.post('/artikel/tulis', data={'judul':'DeCare', 'isi':'blahblahblah'})
        self.assertRedirects(response, '/artikel/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)


    def test_url_detail_artikel(self):
        self.client.login(username='tester', password='password')
        form = TulisArtikel(data={'judul':'Mantap', 'isi':'Ay boss'})
        art = form.save(commit=False)
        art.author = self.user
        art.save()
        response = Client().get('/artikel/detail/mantap/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)


    def test_url_detail_artikel_using_func(self):
        self.client.login(username='tester', password='password')
        form = TulisArtikel(data={'judul':'Mantap', 'isi':'Ay boss'})
        art = form.save(commit=False)
        art.author = self.user
        art.save()
        found = resolve('/artikel/detail/mantap/')
        self.assertEqual(found.func, show_artikel)

    def test_template_view_detail_artikel(self):
        self.client.login(username='tester', password='password')
        form = TulisArtikel(data={'judul':'Mantap', 'isi':'Ay boss'})
        art = form.save(commit=False)
        art.author = self.user
        art.save()
        html_response = Client().get('/artikel/detail/mantap/').content.decode('utf8')
        self.assertIn("Mantap", html_response)
        self.assertIn("Ay boss", html_response)
        self.assertIn("Baca artikel lain", html_response)

# Test untuk comments

    def test_models_comments(self):
        art = Artikel.objects.create(judul='test', isi='blabla', author=self.user)
        comment = Comment.objects.create(artikel=art, author=self.user, body='test')
        self.assertEqual(comment.artikel, art)
        self.assertEqual(comment.body, 'test')
        self.assertEqual(str(comment), 'test - tester')

    def test_post_comment(self):
        self.client.login(username='tester', password='password')
        artikel = Artikel.objects.create(judul='testing', isi='blabla', author=self.user)
        response = self.client.post('/artikel/detail/testing/',data={'body':'ok bro'})
        html_response = response.content.decode('utf8')
        self.assertIn("ok bro", html_response)

    def test_delete_comment(self):
        self.client.login(username='tester', password='password')
        artikel = Artikel.objects.create(judul='testing', isi='blabla', author=self.user)
        self.client.post('/artikel/detail/testing/',data={'body':'ok bro'})
        response = self.client.post('/artikel/delete/1/')
        self.assertEqual(response.status_code, 200)

