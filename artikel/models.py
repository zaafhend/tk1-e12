from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

class Artikel(models.Model):
    slug = models.SlugField()
    judul = models.CharField(max_length=150)
    isi = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE ,default=None)

    def save(self, *args, **kwargs):
        suffix = 0
        candidate = base = slugify(self.judul[:50])
        self.slug = None
        while not self.slug:
            if suffix:
                candidate = f'{base}-{suffix}'
            if not Artikel.objects.filter(slug=candidate).exists():
                self.slug = candidate
            suffix += 1
        super(Artikel, self).save(*args, **kwargs)

    def __str__(self):
        return self.judul

    def snippet(self):
        return self.isi[:150] + '...' if len(self.isi) > 150 else self.isi


class Comment(models.Model):
    artikel = models.ForeignKey(Artikel, on_delete=models.CASCADE ,default=None, related_name="artikel")
    author = models.ForeignKey(User, on_delete=models.CASCADE ,default=None, related_name="author")
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s - %s' % (self.body, self.author)