from django.urls import path
from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.artikel, name='index'),
    path('tulis', views.tulis, name='tulis'),
    path('detail/<slug:slug>/', views.show_artikel, name='show_artikel'),
    path('delete/<int:id>/', views.delete_comment, name='delete_comment'),
]
