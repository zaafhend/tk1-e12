from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .apps import *
from .models import *

# Create your tests here.
class LandingPageTest(TestCase):

    def test_apps(self):
        self.assertEqual(LandingConfig.name, 'landing')
        self.assertEqual(apps.get_app_config('landing').name, 'landing')

        #untuk polling
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('tester', '', 'password')

    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_function_exists(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing/home.html')

    def test_json_resp_url_belum_voting(self):
        response = Client().get('/hasilpoll')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"sudah": 0, "belum": 0}
        )

    def test_save_poll_sudah(self):
        self.client.login(username='tester', password='password')
        response = self.client.post('/pollsudah')
        counting_poll_sudah = PollModel.objects.all().count()
        self.assertEqual(counting_poll_sudah, 1)

        self.assertEqual(response.status_code, 200)
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1', html_response)
        

    def test_poll_belum_masuk(self):
        self.client.login(username='tester', password='password')
        response = self.client.post('/pollbelum')
        counting_poll_sudah = PollModel.objects.all().count()
        self.assertEqual(counting_poll_sudah, 1)

        self.assertEqual(response.status_code, 200)
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1', html_response)