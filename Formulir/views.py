from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import covidTest
from .forms import formTest
from django.contrib import messages
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def formPage(request):
    formulir = formTest()
    if request.method == "POST":
        formulir = formTest(request.POST)
        if formulir.is_valid():
            formulir.save()
            return render(request, 'respon.html', {})
        else:
            messages.error(request, "Form is invalid")
    
    context = {
        'form' : formulir
    }
    return render(request, 'formulir.html', context)

@login_required(login_url='/login/')
def listForm(request):
    list_formulir = covidTest.objects.all()
    context = {'listForm':list_formulir}
    return render(request, 'list.html', context)