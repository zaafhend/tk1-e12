from django.test import TestCase, Client, override_settings
from django.apps import apps
from django.urls import reverse, resolve
from .views import *
from .models import covidTest
from .forms import formTest
from .apps import FormulirConfig
from django.contrib.auth.models import User
from django.http import request
from django.contrib.messages import get_messages

class FormulirUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('testUser', '', 'password')

    def test_app_is_exist(self):
        self.assertEqual(FormulirConfig.name, 'Formulir')
        self.assertEqual(apps.get_app_config('Formulir').name, 'Formulir')

    # def test_template_used(self):
    #     self.client.login(username='testUser', password='password')
    #     response = Client().get('/Form/')
    #     self.assertTemplateUsed(response, None)

    def test_Form_url_is_exist(self):
        self.client.login(username='testUser', password='password')
        response = Client().get('/Form/')
        self.assertEqual(response.status_code, 302)
    
    def test_listForm_url_is_exist(self):
        self.client.login(username='testUser', password='password')
        response = self.client.get('/Form/listForm')
        self.assertEqual(response.status_code, 301)
    
    def test_url_using_func1(self):
        self.client.login(username='testUser', password='password')
        found = resolve('/Form/')
        self.assertEqual(found.func, formPage)
    
    def test_url_using_func(self):
        found = resolve('/Form/listForm/')
        self.assertEqual(found.func, listForm)

    def test_form(self):
        self.client.login(username='testUser', password='password')
        newForm = covidTest.objects.create(Nama_Lengkap='DeCare', Usia='19', No_Telp='0813212323546', Jenis_Kelamin='Perempuan', Alamat='Depok', Nama_Test='Rapid Test', Lokasi_Test='Aceh')
        countForm = covidTest.objects.all().count()
        self.assertEqual(countForm, 1)

    def test_form_is_blank(self):
        self.client.login(username='testUser', password='password')
        form = formTest(data={'Nama_Lengkap':'', 'Usia':'', 'No_Telp':'', 'Jenis_Kelamin':'', 'Alamat':'', 'Nama_Test':'', 'Lokasi_Test':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['Nama_Lengkap'],
            ["This field is required."]
            )
    
    def test_check_form(self):
        self.client.login(username='testUser', password='password')
        response = Client().get('/Form/')
        content = response.content.decode('utf8')
        self.assertIn('', content)
    
    def test_form_is_valid(self):
        self.client.login(username='testUser', password='password')
        response = Client().get('/Form/')
        form = formTest(data={'Nama_Lengkap':'DeCare', 'Usia':'19', 'No_Telp':'0813212323546', 'Jenis_Kelamin':'Perempuan', 'Alamat':'Depok', 'Nama_Test':'Rapid Test', 'Lokasi_Test':'Aceh'})
        self.assertTrue(form.is_valid())

    def test_form_is_invalid(self):
        self.client.login(username='testUser', password='password')
        response = Client().get('/Form/')
        form = formTest(data={'Nama_Lengkap':'DeCare', 'Usia':'abc', 'No_Telp':'0813212323546', 'Jenis_Kelamin':'Perempuan', 'Alamat':'Depok', 'Nama_Test':'Rapid Test', 'Lokasi_Test':'Aceh'})
        self.assertFalse(form.is_valid())
        # messages = [m.message for m in get_messages(response.wsgi_request)]
        # self.assertIn('Form is invalid', messages)
        # content = response.content.decode('utf8')
        # self.assertIn('Form is invalid', content)

    def test_respon_form(self):
        self.client.login(username='testUser', password='password')
        response = Client().post('/Form/', data={'Nama_Lengkap':'DeCare', 'Usia':'19', 'No_Telp':'0813212323546', 'Jenis_Kelamin':'Perempuan', 'Alamat':'Depok', 'Nama_Test':'Rapid Test', 'Lokasi_Test':'Aceh'})
        self.assertEqual(response.status_code, 302)
    
    def test_template_used_form(self):
        self.client.login(username='testUser', password='password')
        # newForm = covidTest.objects.create(Nama_Lengkap='DeCare', Usia='19', No_Telp='0813212323546', Jenis_Kelamin='Perempuan', Alamat='Depok', Nama_Test='Rapid Test', Lokasi_Test='Aceh')
        response = self.client.get('/Form/')
        self.assertTemplateUsed(response, 'formulir.html')

    def test_template_used_list(self):
        self.client.login(username='testUser', password='password')
        # newForm = covidTest.objects.create(Nama_Lengkap='DeCare', Usia='19', No_Telp='0813212323546', Jenis_Kelamin='Perempuan', Alamat='Depok', Nama_Test='Rapid Test', Lokasi_Test='Aceh')
        response = self.client.get('/Form/listForm/')
        self.assertTemplateUsed(response, 'list.html')
    
    def test_post_form(self):
        self.client.login(username='testUser', password='password')
        response = Client().post('/Form/',{
            'Nama_Lengkap':'',
            'Usia':'',
            'No_Telp':'',
            'Jenis_Kelamin':'',
            'Alamat':'',
            'Nama_Test':'',
            'Lokasi_Test':'',
        })
        self.assertIn('', response.content.decode())
    
    def test_get_Data(self):
        covidTest.objects.create(Nama_Lengkap='DeCare', Usia='19', No_Telp='0813212323546', Jenis_Kelamin='Perempuan', Alamat='Depok', Nama_Test='Rapid Test', Lokasi_Test='Aceh')
        data = covidTest.objects.get(Nama_Lengkap='DeCare')
        self.assertEqual(str(data), 'DeCare')