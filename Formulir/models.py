from django.db import models

class covidTest(models.Model):
    Gender = (
        ('', 'Choose..'),
        ('Laki-laki', 'Laki-laki'),
        ('Perempuan', 'Perempuan'),
    )
    Namatest = (
        ('', 'Choose..'),
        ('Rapid Test', 'Rapid Test'),
        ('Swab Test', 'Swab Test'),
    )
    Provinsi = (
        ('', 'Provinsi'),
        ('Aceh', 'Aceh'),
        ('Bali', 'Bali'),
        ('Banten', 'Banten'),
        ('Bengkulu', 'Bengkulu'),
        ('DI Yogyakarta', 'DI Yogyakarta'),
        ('DKI Jakarta', 'DKI Jakarta'),
        ('Gorontalo', 'Gorontalo'),
        ('Jambi', 'Jambi'),
        ('Jawa Barat', 'Jawa Barat'),
        ('Jawa Tengah', 'Jawa Tengah'),
        ('Jawa Timur', 'Jawa Timur'),
        ('Kalimantan Barat', 'Kalimantan Barat'),
        ('Kalimantan Selatan', 'Kalimantan Selatan'),
        ('Kalimantan Tengah', 'Kalimantan Tengah'),
        ('Kalimantan Timur', 'Kalimantan Timur'),
        ('Kalimantan Utara', 'Kalimantan Utara'),
        ('Kepulauan Belitung', 'Kepualauan Belitung'),
        ('Kepulauan Riau', 'Kepulauan Riau'),
        ('Lampung', 'Lampung'),
        ('Maluku', 'Maluku'),
        ('Maluku Utara', 'Maluku Utara'),
        ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
        ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
        ('Riau', 'Riau'),
        ('Sulawesi Barat', 'Sulawesi Barat'),
        ('Sulawesi Selatan', 'Sulawesi Selatan'),
        ('Sulawesi Tengah', 'Sulawesi Tengah'),
        ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
        ('Sulawesi Utara', 'Sulawesi Utara'),
        ('Sumatera Barat', 'Sumatera Barat'),
        ('Sumatera Selatan', 'Sumatera Selatan'),
        ('Sumatera Utara', 'Sumatera Utara'),
        ('Papua', 'Papua'),
        ('Papua Barat', 'Papua Barat'),
    )

    Nama_Lengkap = models.CharField(max_length=70)
    Usia = models.PositiveIntegerField()
    No_Telp = models.PositiveIntegerField()
    Jenis_Kelamin = models.CharField(max_length=10, choices=Gender, default='')
    Alamat = models.TextField(max_length=150)
    Nama_Test = models.CharField(max_length=20, choices=Namatest, default='')
    Lokasi_Test = models.CharField(max_length=50, choices=Provinsi, default='')
        
    def __str__(self):
        return self.Nama_Lengkap