from django import forms
from .models import covidTest

class formTest(forms.ModelForm):
    class Meta:
        model = covidTest
        fields = ('Nama_Lengkap', 'Usia', 'No_Telp', 'Jenis_Kelamin', 'Alamat', 'Nama_Test', 'Lokasi_Test')
        widgets = {
            'Nama_Lengkap' : forms.TextInput(attrs={'class': 'form-control', 'placeholder' : 'Nama Lengkap'}),
            'Usia' : forms.TextInput(attrs={'class': 'form-control', 'placeholder' : '00'}),
            'No_Telp' : forms.TextInput(attrs={'class': 'form-control', 'placeholder' : '08123456789'}),
            'Jenis_Kelamin' : forms.Select(attrs={'class': 'form-control formselect'}),
            'Alamat' : forms.Textarea(attrs={'class': 'form-control textForm', 'placeholder' : 'jl. nama jalan, kecamatan, kota, kode pos', 'rows':5}),
            'Nama_Test' : forms.Select(attrs={'class': 'form-control formselect'}),
            'Lokasi_Test' : forms.Select(attrs={'class': 'form-control formselect'}),
        }