from django.urls import path
from . import views

app_name = "Formulir"
urlpatterns = [
    path('', views.formPage, name="formulir"),
    path('listForm/', views.listForm, name="listForm"),
]