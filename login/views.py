from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import UserRegister
from django.http import JsonResponse
from django.contrib.auth.models import User

def register(request):
    if request.method == 'POST':
        form = UserRegister(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Account Succesfully Registered')
            return redirect('/login/')
    else:    
        form = UserRegister()
    return render(request, 'login/register.html', {'form': form})

def check_username(request):
    username = request.GET.get('username', None)
    data = {
        'username_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)

@login_required
def profile(request):
    return render(request, 'login/profile.html')
