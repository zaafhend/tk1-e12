$(document).ready(function(){
   $("small").hide();

    $('#id_password1, #id_password2').keyup(function () {

        if ($('#id_password1').val() == $('#id_password2').val()) {
          $('#id_password2').css({"border-color": "green", "border-width": "medium"});
        }else 
          $('#id_password2').css({"border-color": "red", "border-width": "medium"});
          $('#submit_register').prop('disabled', true);
        
          if ($('#id_password1').val() == $('#id_password2').val()) {
            $('#submit_register').prop('disabled', false);
          }
    });

    $('#reg_req').click(function(){
      $("small").toggle();
    });

    $("#id_username").change(function () {
      var username = $(this).val();

      $.ajax({
        url: '/check_username/',
        data: {
          'username': username
        },
        dataType: 'json',
        success: function (data) {
          if (data.username_taken) {
            $('#id_username').css({"border-color": "red", "border-width": "medium"});
            $('#div_id_username').append('<p id="username_taken" > Username is already taken </p> ');
          }else{
            $('#id_username').css({"border-color": "", "border-width": "thin"});
            $("#username_taken").remove();
          }
        }
      });
    });

});