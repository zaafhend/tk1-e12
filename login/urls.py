from django.urls import path
from django.conf.urls import url
from django.contrib.auth import views as authviews
from . import views

app_name = 'login'

urlpatterns = [
    path('newuser/', views.register, name='register'),
    path('login/', authviews.LoginView.as_view(template_name='login/login.html'), name='login'),
    path('logout/', authviews.LogoutView.as_view(template_name='login/logout.html'), name='logout'),
    path('profile/', views.profile, name='profile'),
    path('check_username/', views.check_username, name='check_username')
]