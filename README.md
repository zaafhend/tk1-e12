# tk2_e12
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]
[Link Heroku][link-heroku]

## Anggota Kelompok

- Arllivandhya Dani / 1906398521
- Najmi Aliya / 1906398622
- Andre Septiano / 1906398313
- Ahmadar Rafi Moreno / 1906398540
- Zaafira Rihhadatul'aisy Hendrarto / 1906353946

## DeCare
DeCare merupakan sebuah aplikasi yang  kita buat dengan fungsi utama sebagai sarana untuk dapat mendaftar test swab secara daring dan berkonsultasi dengan tim medis. Dengan app ini, diharapkan pengguna dapat melakukan  pendaftaran secara online, mendapat informasi mengenai covid, serta menulis blog/artikel pengalaman atau tips atau apa-apa saja yang dapat berguna bagi pembaca lain

## Fitur yang diimplementasikan
 - Login ke akun pengguna
 - Survey pengalaman melakukan rapid test / swab test untuk umum (data langsung ditampilkan).
 - Pendaftaran rapid test / swab test. User dapat melihat antrian pendaftar.
 - Sistem Konsultasi Dokter dengan sistem forum tanya jawab.
 - Blog/artikel dari user yang menceritakan pengalaman seputar Covid.

[link-heroku]: https://decare2.herokuapp.com
[commits-gh]: https://gitlab.com/vandhya/tk2-e12/commits/master
[pipeline-badge]: https://gitlab.com/vandhya/tk2-e12/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/vandhya/tk2-e12/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/vandhya/tk2-e12/-/commits/master